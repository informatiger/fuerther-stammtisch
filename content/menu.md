---
title: "Getränke- und Speisekarten"
date: 2019-02-04T20:34:58+01:00
---

## Speisekarte
![Speisekarte](/img/menu_0.jpg)

## Vegetarische / Vegane Patties
![Vegetarische / Vegane Patties](/img/menu_1.jpg)

## Getränke
![Getränke](/img/menu_2.jpg)
![Getränke](/img/menu_3.jpg)