---
title: "Fürther Fusseltisch #1"
date: 2019-02-04T19:43:44+01:00
description: "Der erste Fürther Fussel Stammtisch"
author: "IceTiger"
best: true

draft: true
---

### Wann?
Der Stammtisch findet am 00.00.2019 von 20:30 bis 22:00 statt.


### Anmeldung
Bitte bis spätestens zum 00.00.2019 anmelden.
Entweder in der Telegram [Gruppe](https://tilnk.de/fft-group) oder per Mail an [IceTiger](mailto:kontakt@icetiger.eu&subject:FFT-#%20Anmeldung).

In Telegram bitte den Hashtag `#FFT-#` hinzufügen, damit Anmeldungen besser sichtbar sind.

### Wo?
__Burger 'n' Friends__.

Schwabacher Str. 131, 90763 Fürth

### Speisen und Getränke

Die Speise und Getränkekarte ist [hier](/menu) einzusehen.
